require "bundler/setup"

require "sinatra"

require "haml"

require "digest/sha1"

require "aws/s3"
require "net/http"

require "data_mapper"
require_relative "classes.rb"
# ---------- BEGIN APP ----------

use Rack::Session::Pool
#set :session_secret, ENV["SSESSION"] || "d7b90fbadf3aed5a136beb51f079c01b"
set :server, :trinidad
set :haml, :format => :html5

DataMapper::Logger.new("#{Dir.pwd}/log.log", 0)
DataMapper.setup(:default,  "sqlite3://#{Dir.pwd}/databases/main.db")
DataMapper.setup(:comments, "sqlite3://#{Dir.pwd}/databases/comments.db")

include AWS::S3
AWS::S3::Base.establish_connection!(
	:access_key_id          => "AKIAJMEGWZ4YH2EEMRHQ",
	:secret_access_key      => "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"
)

class Upload
	attr_accessor :file, :filename, :filename_ogg, :status, :title, :desc, :url
	
	def initialize(file)
		@file = file
		@filename = @file[:filename].gsub(" ", "")
		@filename_ogg = "#{@filename}.ogg"
	end
	
	def downandup
		File.open("uploads/"+@filename, "w") do |file|
			file.write(@file[:tempfile].read)

			@status = "Uploading"
			
			t = Thread.new do
				convert @filename, @filename_ogg
			end
			t.join
		end
	end
	
	def convert(file, file_ogg)
		if file[-4..-1].eql?(".ogg") then
			@status = "No need to convert - already a .ogg!"
			upload @filename, true
		else
			@status = "Converting"
			`avconv -i uploads/#{file} -y -c:a libvorbis -b:a 256k temp/#{file_ogg}`
			@status = "Converted!"
			# Remvove from directory
			`rm -f uploads/#{file}`
			upload @filename_ogg
		end
	end
	
	def upload(file_ogg, ogg = false)
		@status = "Storing"
		Thread.new do
			if ogg == false then
				S3Object.store(file_ogg,
					       open("temp/#{file_ogg}"),
					       "jt-audio",
					       :access => :public_read
				)
			else
				S3Object.store(file_ogg,
				               open("uploads/#{file_ogg}"),
				               "jt-audio",
				               :access => :public_read
				)
			end
		end
		@status = "Stored!"
		@url = "http://s3.amazonaws.com/jt-audio/#{file_ogg}"
		puts @url + " Before Recording.create"
		uot = "temp"
		uot = "uploads" if ogg == true
		Recording.create(
			:title => @title,
			:description => @desc,
			:author_id => Random.rand(5),
			:time_uploaded => Time.now,
			:filename_ogg => @url,
			:upload_or_temp => uot,
			:temp_ogg_exist => true,
			:comments_table => Random.rand(44)
		)
		puts @url + " After Recording.create"
		
	end
end

class UserSession
	attr_accessor :logged_in
	
	def initialize
		@logged_in = false
	end
	
	def logged_in?
		@logged_in
	end
end

$upload = nil
$session = UserSession.new

get "/" do
	haml :upload
end


post "/" do
	$upload = Upload.new(params["audio"])
	$upload.title = params["title"]
	$upload.desc = params["desc"]
	$upload.downandup
end

get "/status" do
	"#{$upload.status}"
end


get "/url" do
	"<a href=\"#{$upload.url}\" target=\"_blank\">#{$upload.url}</a>"
end

get "/rec/:id" do
	id = params[:id]
	@rec = Recording.get(id)
	haml :rec
end

get "/login" do
	$session.logged_in = true
	"#{$session.logged_in}"
end

get "/logout" do
	$session.logged_in = false
	"#{$session.logged_in}"
end

get "/logged_in" do
	"#{$session.logged_in}"
end

get "/newuser" do
	if $session.logged_in == true
		redirect "/"
	else
		haml :usercreate
	end
end

post "/newuser" do
	puts "Name:\t\t#{params[:name]}"
	puts "Username:\t#{params[:username]}"
	puts "Email:\t\t#{params[:email]}"
	
	n = (0..9999).to_a.shuffle!
	r = Random.rand(0..9999)
	User.create(
		:name 		=> params[:name],
		:username	=> params[:username],
		:email		=> params[:email],
		:time_created	=> Time.now,
		:code		=> Digest::SHA1.hexdigest(n[r].to_s),
		:theme		=> 0,
		:photo_url	=> "nothing yet",
		:description	=> "nothing yet",
		:favorite_id	=> 0
	)
end