var isfile = false;
var $form = new FormData();
var $file;
var interval;

$(function() { 
	$("#loading").hide();
	$("#logout").hide();
	$.get("/logged_in", function(data) {
		if (data ==  "false") {}
		else {
			$("#login").html("Welcome!");
			$("#logout").show();
		}
	});
});

$("#choose").on("change", function() {
		isfile = true;
		
		var file = this.files[0];
		  //filename = this.files[0].name;
		  //filesize = this.files[0].size;
		
		var filename = file.name;
		var pos = filename.lastIndexOf(".");
		var ext = filename.substr(pos);
		var status = $("#type");
		switch (ext) {
				case ".ogg":
					status.html("Detected .ogg");
					break;
				case ".wav":
					status.html("Detected .wav");
					break;
				case ".mp3":
					status.html("Detected .mp3");
					break;
				case ".m4a":
					status.html("Detected .m4a");
					break;
				default:
					status.html("Not a valid audio file!");
					isfile = false;
					break;
		}  
		
		$file = file;
});

function getURL() {
	$.get("/url", function(data) {
		$("#url").html(data);
	});
}

function getStatus() {
	$.get("/status", function(data) {
		if (data == "Stored!") {
			clearInterval(interval);
			getURL();
			$("#loading").fadeOut(3000);
		}
		$("#status").html(data);
	});
}

$("#login").on("click", function() {
	if ($("#login").html() != "Welcome!") {
		$.get("/login", function(data) {
			if (data == "true") {
				$("#login").html("Welcome!");
				$("#logout").show();
			}
		});
	}
});

$("#logout").on("click", function() {
	$.get("/logout", function(data) {
		if (data == "false") {
			$("#logout").hide();
			$("#login").html("Login!");
		}
	});
});

$("#upload").on("click", function() {
	if(isfile == true) {
		$.get("/logged_in", function(data) {
			if (data == "false"){
				alert("Sorry, but you have to be logged in.");
			} else {
				upload();
			}
		});
	} else {
		alert("No file selected!");
	}
});

function upload() {
	$("#tohide").fadeOut(1000);
	$("#loading").fadeIn(2500);
	
	$form.append($("#choose").attr("name"), $file);
	$form.append($("#title").attr("name"),  $("#title").val());
	$form.append($("textarea").attr("name"), $("textarea").val());
	var xhr = new XMLHttpRequest();
	xhr.upload.addEventListener("progress", update, false); 
	xhr.open("POST", "/");
	xhr.send($form);

	setTimeout(function() {
		interval = setInterval(getStatus, 250);
	}, 250);
}

function update(e) {
	var p = e.loaded*100/e.total;
	if (e.lengthComputable) {
		$("#progress").html("" + (e.loaded*100/e.total) + "%");
		if (p == 100) {
			$("#progress").fadeOut(1000);
		}
	} else {
		$("#progress").html(e);
	}
}
