require "bundler/setup"
require "sinatra"

set :server, :thin

get "/multithread" do
  Thread.new {
    #puts "sleeping for 10 sec"
    #sleep 10
    # Actually make a call to Third party API using HTTP NET or whatever.
    filename = "Flux_Pavillion_I_Cant_Stop.wav"
    `avconv -i uploads/#{filename} -y -c:a libvorbis -b:a 256k temp/#{filename}.ogg`
  }
  #t1.join
  "multi thread"
end

get "/dummy" do
	"#{Random.rand(1..100)}"
end
