require 'data_mapper'

DataMapper.setup(:default,  "sqlite3://#{Dir.pwd}/databases/main.db")
DataMapper.setup(:comments, "sqlite3://#{Dir.pwd}/databases/comments.db")

class Recording
	include DataMapper::Resource

	property :id,			Serial
	property :author_id,		Integer
	property :description,		Text,		:lazy => false
	property :title,		String
	property :filename_ogg,		String,		:length => 250
	property :temp_ogg_exist,	Boolean
	property :upload_or_temp,	String
	property :time_uploaded,	DateTime
	property :comments_table,	Integer
	
	#belongs_to :user
	#has n, :comments
end

class User
	include DataMapper::Resource
	
	property :id,			Serial
	property :name,			String
	property :username, 		String
	# An email will be sent each time a user logs in with a new code, considered.
	# Might add an option for a set password to go along with this in the future.
	property :code,			String
	property :theme,		Integer
	property :time_created,		DateTime
	property :email,		String
	property :photo_url,		String
	property :description,		Text
	# This was to favorite certain Users, so an Integer for the User's Serial
	property :favorite_id,		Integer
	
	#has n, :recordings
end

class Favorite
	include DataMapper::Resource
	
	property :dumb, 	Serial
	property :user,		Integer
	property :recording_id,	Integer
end

class Audience
	include DataMapper::Resource
	
	property :dumb,		Serial
	property :following_id, Integer
	property :follower_id, 	Integer
end

# -------- ITS OWN DATABASE --------
class Comment
	include DataMapper::Resource
	
	def self.default_repository_name
		:comments
	end
	
	property :table_id,		Serial
	property :id,			Integer
	property :time,			DateTime
	property :commenter,		String
	property :comment,		Text
	property :reply_to_id,		Integer
	
	#belongs_to :recording
end

DataMapper.finalize
# Use the one below to do a major change, but it wipes data: changing or removing properties
#DataMapper.auto_migrate!
# Use the one below to add a property to the database without wiping data, but you can't change or remove a property
DataMapper.auto_upgrade!